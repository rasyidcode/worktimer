package com.mcsnowflake.worktimer.data

import io.kotest.core.spec.Spec
import io.kotest.core.spec.style.StringSpec
import io.kotest.matchers.shouldBe
import kotlin.time.ExperimentalTime
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.FlowPreview
import kotlinx.coroutines.Job
import kotlinx.coroutines.ObsoleteCoroutinesApi
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.test.TestCoroutineDispatcher
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.runBlockingTest
import kotlinx.coroutines.test.setMain

@ObsoleteCoroutinesApi
@ExperimentalTime
@FlowPreview
@ExperimentalCoroutinesApi
class BroadcasterTest : StringSpec() {

    private val testDispatcher = TestCoroutineDispatcher() // the dispatcher

    override fun beforeSpec(spec: Spec) {
        super.beforeSpec(spec)
        Dispatchers.setMain(testDispatcher) // set the coroutine context
    }

    override fun afterSpec(spec: Spec) {
        super.afterSpec(spec)
        Dispatchers.resetMain() // reset
        testDispatcher.cleanupTestCoroutines() // clear all
    }

    init {
        "Broadcaster sends updates to all subscribers" {
            testDispatcher.runBlockingTest {

                val sut = Broadcaster<Int>(this)

                val results = mutableListOf<Int>()
                val jobs = mutableListOf<Job>()
                repeat(3) {
                    jobs.add(
                        sut.subscribe()
                            .onEach { event -> results.add(event) }
                            .launchIn(this)
                    )
                }

                sut.receive(1)

                jobs.forEach { it.cancel() }

                results shouldBe listOf(1, 1, 1)
            }
        }
        "a canceling subscriber doesn't cancel others" {
            testDispatcher.runBlockingTest {
                val sut = Broadcaster<Int>(this)

                val result1 = mutableListOf<Int>()
                val result2 = mutableListOf<Int>()
                val jobs = mutableListOf<Job>()

                jobs.add(
                    sut.subscribe()
                        .onEach { result1.add(it) }
                        .launchIn(this)
                )
                jobs.add(
                    sut.subscribe()
                        .onEach { result2.add(it) }
                        .launchIn(this)
                )
                sut.receive(1)
                jobs[1].cancel()

                sut.receive(2)

                delay(10)
                jobs[0].cancel()

                result1.size shouldBe 2
                result1[1] shouldBe 2

                result2.size shouldBe 1
                result2[0] shouldBe 1
            }
        }
        "New subscribers get the latest event (if any)" {
            testDispatcher.runBlockingTest {
                val sut = Broadcaster<Int>(this)

                val result1 = mutableListOf<Int>()
                val result2 = mutableListOf<Int>()
                val jobs = mutableListOf<Job>()

                jobs.add(sut.subscribe().onEach { result1.add(it) }.launchIn(this))

                sut.receive(1)
                sut.receive(2)

                jobs.add(sut.subscribe().onEach { result2.add(it) }.launchIn(this))

                delay(10)
                jobs.forEach { it.cancel() }

                result1.size shouldBe 2
                result1[0] shouldBe 1

                result2.size shouldBe 1
                result2[0] shouldBe 2
            }
        }
    }
}
