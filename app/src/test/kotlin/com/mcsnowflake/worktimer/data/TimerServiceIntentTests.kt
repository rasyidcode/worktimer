package com.mcsnowflake.worktimer.data

import android.content.Intent
import com.mcsnowflake.worktimer.TimerService
import com.mcsnowflake.worktimer.model.TimerEvent
import com.mcsnowflake.worktimer.notifications.NotificationFactory
import io.kotest.core.spec.Spec
import io.kotest.core.spec.style.StringSpec
import io.kotest.koin.KoinListener
import io.mockk.*
import kotlin.time.ExperimentalTime
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.FlowPreview
import kotlinx.coroutines.test.TestCoroutineDispatcher
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.runBlockingTest
import kotlinx.coroutines.test.setMain
import org.koin.dsl.module

@FlowPreview
@ExperimentalTime
@ExperimentalCoroutinesApi
class TimerServiceIntentTests : StringSpec() {

    private val testDispatcher = TestCoroutineDispatcher() // the dispatcher

    override fun beforeSpec(spec: Spec) {
        super.beforeSpec(spec)
        Dispatchers.setMain(testDispatcher) // set the coroutine context
    }

    override fun afterSpec(spec: Spec) {
        super.afterSpec(spec)
        Dispatchers.resetMain() // reset
        testDispatcher.cleanupTestCoroutines() // clear all
    }

    private var testBroadcaster = mockk<Broadcaster<TimerEvent>>()
    private var notFactory = mockk<NotificationFactory>()
    private var testTimer = mockk<WorkTimer>()

    private val appModule = module {
        single { testBroadcaster }
        single { testTimer }
        single { notFactory }
    }

    override fun listeners() = listOf(KoinListener(appModule))

    init {

        "START intent should configure and start timer" {
            testDispatcher.runBlockingTest {

                val testIntent = mockk<Intent>()
                every { testIntent.action } returns TimerService.Action.START.name

                testTimer = mockk<WorkTimer>()
                coEvery { testTimer.start() } just Runs

                val sut = TimerService(this)

                sut.onStartCommand(testIntent, 0, 0)

                coVerify(exactly = 1) { testTimer.start() }
            }
        }

        "NEXT intent should stop, toggle state of and restart timer" {
            testDispatcher.runBlockingTest {

                val testIntent = mockk<Intent>()
                every { testIntent.action } returns TimerService.Action.NEXT.name

                testTimer = mockk<WorkTimer>()
                coEvery { testTimer.nextState() } just Runs

                val sut = TimerService(this)

                sut.onStartCommand(testIntent, 0, 0)

                coVerify(exactly = 1) { testTimer.nextState() }
            }
        }

        "MORE intent should prolong timer" {
            testDispatcher.runBlockingTest {

                val testIntent = mockk<Intent>()
                every { testIntent.action } returns TimerService.Action.MORE.name

                testTimer = mockk<WorkTimer>()
                coEvery { testTimer.moreTime() } just Runs

                val sut = TimerService(this)

                sut.onStartCommand(testIntent, 0, 0)

                coVerify(exactly = 1) { testTimer.moreTime() }
            }
        }

        "STOP intent should stop service" {
            testDispatcher.runBlockingTest {

                val testIntent = mockk<Intent>()
                every { testIntent.action } returns TimerService.Action.STOP.name
                testTimer = mockk<WorkTimer>()
                coEvery { testTimer.stop() } just Runs

                val sut = spyk(TimerService(this))

                sut.onStartCommand(testIntent, 0, 0)

                verify(exactly = 1) { sut.stopSelf() }
                coVerify(exactly = 1) { testTimer.stop() }
            }
        }
    }
}
