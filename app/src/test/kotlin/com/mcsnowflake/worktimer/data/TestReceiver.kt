package com.mcsnowflake.worktimer.data

import com.mcsnowflake.worktimer.model.TimerEvent
import kotlin.time.ExperimentalTime
import kotlinx.coroutines.ExperimentalCoroutinesApi

@ExperimentalTime
@ExperimentalCoroutinesApi
class TestReceiver : EventReceiver<TimerEvent> {

    val receivedEvents = mutableListOf<TimerEvent>()

    override suspend fun receive(event: TimerEvent) {
        receivedEvents.add(event)
    }
}
