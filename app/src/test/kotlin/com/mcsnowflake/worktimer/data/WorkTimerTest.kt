package com.mcsnowflake.worktimer.data

import com.mcsnowflake.worktimer.data.WorkTimer.State
import com.mcsnowflake.worktimer.data.WorkTimer.State.*
import com.mcsnowflake.worktimer.model.TimerConfig
import com.mcsnowflake.worktimer.model.TimerEvent
import io.kotest.assertions.assertSoftly
import io.kotest.core.spec.Spec
import io.kotest.core.spec.style.StringSpec
import io.kotest.extensions.time.withConstantNow
import io.kotest.matchers.shouldBe
import java.time.LocalDateTime
import kotlin.time.ExperimentalTime
import kotlin.time.seconds
import kotlinx.coroutines.*
import kotlinx.coroutines.test.TestCoroutineDispatcher
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.runBlockingTest
import kotlinx.coroutines.test.setMain

@FlowPreview
@ExperimentalTime
@ExperimentalCoroutinesApi
class WorkTimerTest : StringSpec() {

    private val testDispatcher = TestCoroutineDispatcher() // the dispatcher

    override fun beforeSpec(spec: Spec) {
        super.beforeSpec(spec)
        Dispatchers.setMain(testDispatcher) // set the coroutine context
    }

    override fun afterSpec(spec: Spec) {
        super.afterSpec(spec)
        Dispatchers.resetMain() // reset
        testDispatcher.cleanupTestCoroutines() // clear all
    }

    init {
        "Timer starts in state working and switches correctly to long and short breaks and back" {
            testDispatcher.runBlockingTest {

                val testScope = CoroutineScope(testDispatcher)
                val testReceiver = TestReceiver()
                val testConfig = TimerConfig(
                    1.seconds,
                    1.seconds,
                    1.seconds,
                    1,
                    overTime = 1.seconds
                )
                val testObject = WorkTimer(testReceiver, testConfig, testScope)

                val testStart = LocalDateTime.now()
                withConstantNow(testStart) {
                    testObject.start()

                    assertSoftly {

                        testReceiver.receivedEvents.runNextStateTest(1, WORK_SESSION)

                        mapOf(
                            4 to SHORT_BREAK,
                            7 to WORK_SESSION,
                            10 to LONG_BREAK,
                            13 to WORK_SESSION,
                            16 to SHORT_BREAK,
                            19 to WORK_SESSION,
                            22 to LONG_BREAK
                        ).forEach { (position, expState) ->
                            testObject.nextState()
                            testReceiver.receivedEvents.runNextStateTest(position, expState)
                        }
                    }
                }
                testScope.cancel()
            }
        }

        "Timer sends updates every second" {
            testDispatcher.runBlockingTest {

                val testScope = CoroutineScope(testDispatcher)

                val nrOfSeconds = 20
                val testReceiver = TestReceiver()
                val testConfig = TimerConfig(
                    nrOfSeconds.seconds,
                    5.seconds,
                    5.seconds,
                    7,
                    overTime = 1.seconds
                )

                val testObject = WorkTimer(testReceiver, testConfig, testScope)

                val testStart = LocalDateTime.now()
                withConstantNow(testStart) {
                    testObject.start()
                }

                advanceTimeBy(500)

                assertSoftly {
                    testReceiver.receivedEvents.size shouldBe 2
                    (testReceiver.receivedEvents[1] is TimerEvent.Progress) shouldBe true
                    (testReceiver.receivedEvents[1] as TimerEvent.Progress).progress shouldBe 0
                    (testReceiver.receivedEvents[1] as TimerEvent.Progress).remainingTime shouldBe nrOfSeconds.seconds
                    (1 until nrOfSeconds).forEach {
                        withConstantNow(testStart.plusSeconds(it.toLong())) {
                            advanceTimeBy(1000)
                        }
                        testReceiver.receivedEvents.size shouldBe it + 2
                        (testReceiver.receivedEvents[it + 1] is TimerEvent.Progress) shouldBe true
                        (testReceiver.receivedEvents[it + 1] as TimerEvent.Progress).progress shouldBe it * 100 / nrOfSeconds
                        (testReceiver.receivedEvents[it + 1] as TimerEvent.Progress).remainingTime shouldBe (nrOfSeconds - it).seconds
                    }
                    withConstantNow(testStart.plusSeconds(nrOfSeconds.toLong())) {
                        advanceTimeBy(1000)
                    }
                    testReceiver.receivedEvents.size shouldBe nrOfSeconds + 3
                    (testReceiver.receivedEvents[nrOfSeconds + 1] is TimerEvent.Progress) shouldBe true
                    (testReceiver.receivedEvents[nrOfSeconds + 1] as TimerEvent.Progress).progress shouldBe 100
                    (testReceiver.receivedEvents[nrOfSeconds + 2] is TimerEvent.Finished) shouldBe true
                }

                testScope.cancel()
            }
        }

        "Timer prolongs upon moreTime() request for the overTime configured in config" {
            testDispatcher.runBlockingTest {

                val testScope = CoroutineScope(testDispatcher)
                val testReceiver = TestReceiver()
                val testConfig = TimerConfig(
                    1.seconds,
                    2.seconds,
                    5.seconds,
                    7,
                    overTime = 1.seconds
                )

                val testObject = WorkTimer(testReceiver, testConfig, testScope)

                val testStart = LocalDateTime.now()
                withConstantNow(testStart) {
                    testObject.start()
                }

                assertSoftly {
                    testReceiver.receivedEvents.size shouldBe 2
                    (testReceiver.receivedEvents[1] is TimerEvent.Progress) shouldBe true
                    (testReceiver.receivedEvents[1] as TimerEvent.Progress).progress shouldBe 0

                    withConstantNow(testStart) {
                        testObject.moreTime()
                    }
                    withConstantNow(testStart.plusSeconds(1)) {
                        advanceTimeBy(1000)
                    }
                    testReceiver.receivedEvents.size shouldBe 3
                    (testReceiver.receivedEvents[2] is TimerEvent.Progress) shouldBe true
                    (testReceiver.receivedEvents[2] as TimerEvent.Progress).progress shouldBe 50

                    withConstantNow(testStart.plusSeconds(2)) {
                        advanceTimeBy(1000)
                    }
                    testReceiver.receivedEvents.size shouldBe 5

                    (testReceiver.receivedEvents[3] is TimerEvent.Progress) shouldBe true
                    (testReceiver.receivedEvents[3] as TimerEvent.Progress).progress shouldBe 100
                    (testReceiver.receivedEvents[4] is TimerEvent.Finished) shouldBe true
                }

                testScope.cancel()
            }
        }

        "Timer restarts with added overTime upon moreTime() request if timer finished before" {
            testDispatcher.runBlockingTest {

                val testScope = CoroutineScope(testDispatcher)
                val testReceiver = TestReceiver()
                val testConfig = TimerConfig(
                    0.seconds,
                    2.seconds,
                    5.seconds,
                    7,
                    1.seconds
                )
                val testObject = WorkTimer(testReceiver, testConfig, testScope)

                val testStart = LocalDateTime.now()
                withConstantNow(testStart) {
                    testObject.start()
                }

                assertSoftly {
                    testReceiver.receivedEvents.size shouldBe 3
                    (testReceiver.receivedEvents[2] as TimerEvent.Finished).state shouldBe WORK_SESSION

                    withConstantNow(testStart) {
                        testObject.moreTime()
                    }
                    testReceiver.receivedEvents.size shouldBe 5
                    (testReceiver.receivedEvents[3] as TimerEvent.Started).state shouldBe WORK_SESSION

                    (testReceiver.receivedEvents[4] as TimerEvent.Progress).progress shouldBe 0
                    (testReceiver.receivedEvents[4] as TimerEvent.Progress).state shouldBe WORK_SESSION
                    (testReceiver.receivedEvents[4] as TimerEvent.Progress).remainingTime shouldBe 1.seconds

                    withConstantNow(testStart.plusSeconds(1)) {
                        advanceTimeBy(1000)
                    }

                    testReceiver.receivedEvents.size shouldBe 7

                    (testReceiver.receivedEvents[5] as TimerEvent.Progress).state shouldBe WORK_SESSION
                    (testReceiver.receivedEvents[5] as TimerEvent.Progress).progress shouldBe 100
                    (testReceiver.receivedEvents[6] as TimerEvent.Finished).state shouldBe WORK_SESSION
                }

                testScope.cancel()
            }
        }

        "Timer sends start event upon start" {
            testDispatcher.runBlockingTest {

                val testScope = CoroutineScope(testDispatcher)
                val testReceiver = TestReceiver()
                val testConfig = TimerConfig(
                    1.seconds,
                    1.seconds,
                    1.seconds,
                    1,
                    1.seconds
                )

                val testObject = WorkTimer(testReceiver, testConfig, testScope)

                val testStart = LocalDateTime.now()
                withConstantNow(testStart) {
                    testObject.start()
                }

                assertSoftly {
                    testReceiver.receivedEvents.size shouldBe 2

                    (testReceiver.receivedEvents[0] is TimerEvent.Started) shouldBe true
                    (testReceiver.receivedEvents[0] as TimerEvent.Started).state shouldBe WORK_SESSION
                }

                testObject.stop()
                testScope.cancel()
            }
        }

        "Timer sends finish event when timer finishes" {
            testDispatcher.runBlockingTest {

                val testScope = CoroutineScope(testDispatcher)
                val testReceiver = TestReceiver()
                val testConfig = TimerConfig(
                    0.seconds,
                    1.seconds,
                    1.seconds,
                    1,
                    1.seconds
                )

                val testObject = WorkTimer(testReceiver, testConfig, testScope)

                val testStart = LocalDateTime.now()
                withConstantNow(testStart) {
                    testObject.start()
                }

                assertSoftly {
                    testReceiver.receivedEvents.size shouldBe 3
                    (testReceiver.receivedEvents[2] as TimerEvent.Finished).state shouldBe WORK_SESSION
                }

                testScope.cancel()
            }
        }

        "Timer sends termination event when timer is stopped" {
            testDispatcher.runBlockingTest {

                val testScope = CoroutineScope(testDispatcher)
                val testReceiver = TestReceiver()
                val testConfig = TimerConfig(
                    1.seconds,
                    1.seconds,
                    1.seconds,
                    1,
                    1.seconds
                )

                val testObject = WorkTimer(testReceiver, testConfig, testScope)

                val testStart = LocalDateTime.now()
                withConstantNow(testStart) {
                    testObject.start()
                    testObject.stop()
                }

                assertSoftly {
                    testReceiver.receivedEvents.size shouldBe 3
                    (testReceiver.receivedEvents[2] is TimerEvent.Terminated) shouldBe true
                    (testReceiver.receivedEvents[2] as TimerEvent.Terminated).state shouldBe WORK_SESSION
                }

                testScope.cancel()
            }
        }
    }

    private fun MutableList<TimerEvent>.runNextStateTest(position: Int, expState: State) {
        size shouldBe position + 1
        (get(position) as TimerEvent.Progress).state shouldBe expState
        (get(position) as TimerEvent.Progress).progress shouldBe 0
        (get(position) as TimerEvent.Progress).remainingTime shouldBe 1.seconds
    }
}
