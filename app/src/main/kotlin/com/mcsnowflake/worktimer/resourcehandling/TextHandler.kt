package com.mcsnowflake.worktimer.resourcehandling

import android.text.format.DateUtils
import com.mcsnowflake.worktimer.data.WorkTimer.State.WORK_SESSION
import com.mcsnowflake.worktimer.model.TimerEvent
import com.mcsnowflake.worktimer.model.TimerEvent.*
import kotlin.time.Duration
import kotlin.time.ExperimentalTime
import kotlin.time.toJavaDuration

object TextHandler { // TODO refactor out string representations

    fun getTitle(event: TimerEvent) = when (event) {
        is Started, is Progress ->
            if (event.state == WORK_SESSION) "Work session ongoing"
            else "Relax time"
        is Finished, is Terminated ->
            if (event.state == WORK_SESSION) "Work session over"
            else "Break is over"
    }

    @ExperimentalTime
    fun getInfoText(event: TimerEvent) = when (event) {
        is Started -> "Session starting"
        is Progress -> event.remainingTime.format()
        is Finished, is Terminated ->
            if (event.state == WORK_SESSION) "Time for a break"
            else "Get ready to rock"
    }

    @ExperimentalTime
    private fun Duration.format(): String {
        val builder = StringBuilder()
        builder.append(DateUtils.formatElapsedTime(toJavaDuration().seconds))
        builder.append(
            when {
                inMinutes > 60 -> " h"
                else -> " min"
            }
        )
        return builder.toString()
    }
}
