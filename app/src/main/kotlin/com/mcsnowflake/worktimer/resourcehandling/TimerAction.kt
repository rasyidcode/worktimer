package com.mcsnowflake.worktimer.resourcehandling

import com.mcsnowflake.worktimer.R
import com.mcsnowflake.worktimer.TimerService
import kotlin.time.ExperimentalTime
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.FlowPreview

@ExperimentalCoroutinesApi
@FlowPreview
@ExperimentalTime
enum class TimerAction(val serviceAction: TimerService.Action) {
    PAUSE(TimerService.Action.NEXT),
    WORK(TimerService.Action.NEXT),
    RESUME(TimerService.Action.NEXT),
    STOP(TimerService.Action.STOP),
    MORE(TimerService.Action.MORE);

    companion object {
        fun getActionTitle(action: TimerAction) = when (action) {
            PAUSE -> "Give me a break"
            WORK -> "Onto it!"
            RESUME -> "OK"
            STOP -> "Let's call it a day"
            MORE -> "Just one moment"
        }

        fun getActionIcon(action: TimerAction) = when (action) {
            PAUSE -> R.drawable.baseline_check_circle_outline_24
            WORK -> R.drawable.baseline_check_circle_outline_24
            RESUME -> R.drawable.baseline_highlight_off_24
            STOP -> R.drawable.baseline_history_24
            MORE -> R.drawable.baseline_history_24
        }
    }
}
