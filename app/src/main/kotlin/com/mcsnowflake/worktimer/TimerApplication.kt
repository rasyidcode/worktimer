package com.mcsnowflake.worktimer

import android.app.Application
import com.mcsnowflake.worktimer.notifications.NotificationFactory
import kotlin.time.ExperimentalTime
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.FlowPreview
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin

class TimerApplication : Application() {
    @ExperimentalCoroutinesApi
    @FlowPreview
    @ExperimentalTime
    override fun onCreate() {
        super.onCreate()
        NotificationFactory.createChannelsWith(applicationContext)
        startKoin {
            androidContext(applicationContext)
            modules(appModule)
        }
    }
}
