package com.mcsnowflake.worktimer

import android.content.ComponentName
import android.content.ServiceConnection
import android.os.Bundle
import android.os.IBinder
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.findNavController
import androidx.navigation.ui.NavigationUI
import com.mcsnowflake.worktimer.presentation.ActiveTimerViewModel
import com.mcsnowflake.worktimer.presentation.SetupFragmentDirections
import kotlin.time.ExperimentalTime
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.FlowPreview
import org.koin.androidx.viewmodel.ext.android.viewModel

@ExperimentalCoroutinesApi
@ExperimentalTime
@FlowPreview
class MainActivity : AppCompatActivity() {

    private val serviceVM by viewModel<ActiveTimerViewModel>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        bindService() // sending the binding request here lets it bind as soon as the service is up
        setContentView(R.layout.main_activity)
        NavigationUI.setupActionBarWithNavController(this, findNavController(R.id.nav_host))
    }

    private val connection: ServiceConnection = object : ServiceConnection {
        override fun onServiceConnected(name: ComponentName?, service: IBinder?) {
            val binder = service as TimerService.LocalBinder
            serviceVM.setUpdates(binder.getTimerEvents())
            serviceVM.isTimerServiceRunning.value = true
        }

        override fun onServiceDisconnected(name: ComponentName?) {
            serviceVM.isTimerServiceRunning.value = false
        }

        override fun onBindingDied(name: ComponentName?) {
            super.onBindingDied(name)
            bindService()
        }
    }

    private fun bindService() {
        bindService(TimerService.getIntent(applicationContext), connection, 0)
    }

    override fun onDestroy() {
        super.onDestroy()
        unbindService(connection)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {

        return when (item.itemId) {
            R.id.action_settings -> {
                findNavController(R.id.nav_host).navigate(SetupFragmentDirections.toSettingsFragment())
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }
}
