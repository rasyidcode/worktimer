package com.mcsnowflake.worktimer.data

import android.util.Log
import java.util.function.Consumer
import kotlinx.coroutines.*
import kotlinx.coroutines.channels.awaitClose
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.callbackFlow
import kotlinx.coroutines.sync.Mutex
import kotlinx.coroutines.sync.withLock

class Broadcaster<T : Any>(private val scope: CoroutineScope) : EventReceiver<T> {

    private val mutex = Mutex()

    private lateinit var lastUpdate: T
    private val listeners = arrayListOf<Consumer<T>>()

    override suspend fun receive(event: T) {
        Log.d(javaClass.simpleName, "broadcasting $event")
        lastUpdate = event
        mutex.withLock {
            listeners.forEach {
                it.accept(event)
            }
        }
    }

    @ExperimentalCoroutinesApi
    fun subscribe(): Flow<T> = callbackFlow {
        val listener = Consumer<T> { offer(it) }
        scope.launch { register(listener) }
        awaitClose { scope.launch { unRegister(listener) } }
    }

    private suspend fun register(listener: Consumer<T>) = mutex.withLock {
        listeners.add(listener)
        if (::lastUpdate.isInitialized) listener.accept(lastUpdate)
    }

    private suspend fun unRegister(listener: Consumer<T>) = mutex.withLock {
        listeners.remove(listener)
    }
}

interface EventReceiver<T> {
    suspend fun receive(event: T)
}
