package com.mcsnowflake.worktimer.data

import com.mcsnowflake.worktimer.data.WorkTimer.State.*
import com.mcsnowflake.worktimer.model.TimerConfig
import com.mcsnowflake.worktimer.model.TimerEvent
import com.mcsnowflake.worktimer.model.TimerEvent.Progress
import java.time.Duration.between
import java.time.LocalDateTime
import kotlin.math.roundToInt
import kotlin.time.ExperimentalTime
import kotlin.time.seconds
import kotlin.time.toJavaDuration
import kotlin.time.toKotlinDuration
import kotlinx.coroutines.*
import kotlinx.coroutines.sync.Mutex
import kotlinx.coroutines.sync.withLock

class WorkTimer @ExperimentalTime constructor(
    private val eventReceiver: EventReceiver<TimerEvent>,
    private val config: TimerConfig,
    private val scope: CoroutineScope
) {
    private val mutex = Mutex()

    private lateinit var job: Job
    private lateinit var startTime: LocalDateTime
    private lateinit var endTime: LocalDateTime

    private var repetition = 0
    private var state = WORK_SESSION

    @ExperimentalTime
    suspend fun start() {
        mutex.withLock {
            startTime = LocalDateTime.now()
            endTime = startTime.plus(config.getDurationFor(state).toJavaDuration())
            startCountDown()
        }
    }

    @ExperimentalTime
    suspend fun moreTime() {
        mutex.withLock {
            if (::job.isInitialized && job.isActive)
                endTime = endTime.plus(config.overTime.toJavaDuration())
            else {
                if (!::startTime.isInitialized) startTime = LocalDateTime.now()
                endTime = LocalDateTime.now().plus(config.overTime.toJavaDuration())
                startCountDown()
            }
        }
    }

    @ExperimentalTime
    suspend fun nextState() {
        stop()
        mutex.withLock {
            state = when (state) {
                WORK_SESSION ->
                    if (repetition == config.shortBreakRepetitions) {
                        repetition = 0
                        LONG_BREAK
                    } else {
                        repetition++
                        SHORT_BREAK
                    }
                SHORT_BREAK -> WORK_SESSION
                LONG_BREAK -> WORK_SESSION
            }
        }
        start()
    }

    suspend fun stop() {
        if (::job.isInitialized && job.isActive) {
            job.cancel()
            eventReceiver.receive(TimerEvent.Terminated(state))
        }
    }

    @ExperimentalTime
    private fun startCountDown() {
        job = scope.launch {
            eventReceiver.receive(TimerEvent.Started(state))
            while (LocalDateTime.now().isBefore(endTime)) {

                val progress = (between(startTime, LocalDateTime.now()).toKotlinDuration().inSeconds / between(startTime, endTime).toKotlinDuration().inSeconds)
                    .times(100.0).toInt()

                // round to full second
                val remainingTime = (between(LocalDateTime.now(), endTime).toMillis() / 1000.0).roundToInt().seconds

                eventReceiver.receive(Progress(state, progress, remainingTime))

                delay(1.seconds)
            }
            eventReceiver.receive(Progress(state, 100, 0.seconds))
            eventReceiver.receive(TimerEvent.Finished(state))
        }
    }

    enum class State {
        WORK_SESSION,
        SHORT_BREAK,
        LONG_BREAK;
    }
}
