package com.mcsnowflake.worktimer.model

import com.mcsnowflake.worktimer.data.WorkTimer
import kotlin.time.Duration
import kotlin.time.ExperimentalTime

sealed class TimerEvent(open val state: WorkTimer.State) {

    data class Started(override val state: WorkTimer.State) : TimerEvent(state)

    data class Progress @ExperimentalTime constructor(
        override val state: WorkTimer.State,
        val progress: Int,
        val remainingTime: Duration
    ) : TimerEvent(state)

    data class Finished(override val state: WorkTimer.State) : TimerEvent(state)
    data class Terminated(override val state: WorkTimer.State) : TimerEvent(state)
}
