package com.mcsnowflake.worktimer.model

import android.content.SharedPreferences
import com.mcsnowflake.worktimer.data.WorkTimer.State
import com.mcsnowflake.worktimer.data.WorkTimer.State.*
import kotlin.time.Duration
import kotlin.time.ExperimentalTime
import kotlin.time.minutes

data class TimerConfig @ExperimentalTime constructor(
    private val workSessionDuration: Duration,
    private val shortBreakDuration: Duration,
    private val longBreakDuration: Duration,
    val shortBreakRepetitions: Int,
    val overTime: Duration
) {

    fun getDurationFor(state: State) =
        when (state) {
            WORK_SESSION -> workSessionDuration
            SHORT_BREAK -> shortBreakDuration
            LONG_BREAK -> longBreakDuration
        }

    companion object {

        @ExperimentalTime
        fun SharedPreferences.getConfig() = TimerConfig(
            getInt(WORK_SESSION_DURATION_KEY, DEFAULT_WORK_SESSION_DURATION).minutes,
            getInt(SHORT_BREAK_DURATION_KEY, DEFAULT_SHORT_BREAK_DURATION).minutes,
            getInt(LONG_BREAK_DURATION_KEY, DEFAULT_LONG_BREAK_DURATION).minutes,
            getInt(SHORT_BREAK_REPETITIONS_KEY, DEFAULT_SHORT_BREAK_REPETITIONS),
            getInt(OVERTIME_DURATION_KEY, DEFAULT_OVERTIME_DURATION).minutes
        )

        // durations in minutes
        const val WORK_SESSION_DURATION_KEY = "work_session_duration"
        const val DEFAULT_WORK_SESSION_DURATION = 25
        const val SHORT_BREAK_DURATION_KEY = "short_break_duration"
        const val DEFAULT_SHORT_BREAK_DURATION = 5
        const val LONG_BREAK_DURATION_KEY = "long_break_duration"
        const val DEFAULT_LONG_BREAK_DURATION = 10
        const val SHORT_BREAK_REPETITIONS_KEY = "short_break_repetitions"
        const val DEFAULT_SHORT_BREAK_REPETITIONS = 3
        const val OVERTIME_DURATION_KEY = "overtime_duration"
        const val DEFAULT_OVERTIME_DURATION = 5

        const val OVERTIME_MODE_KEY = "overtime_mode"
        const val DEFAULT_OVERTIME_MODE = false
        const val DND_MODE_KEY = "dnd_mode"
        const val DEFAULT_DND_MODE = false
        const val HYDRATION_MODE_KEY = "hydrate_mode"
        const val DEFAULT_HYDRATION_MODE = true
        const val REACTIVE_SWITCH_MODE_KEY = "reactive_switch_mode"
        const val DEFAULT_REACTIVE_SWITCH_MODE = true
    }
}
