package com.mcsnowflake.worktimer.notifications

import android.app.NotificationManager
import android.app.NotificationManager.INTERRUPTION_FILTER_ALL
import android.content.Context
import androidx.core.content.ContextCompat.getSystemService
import com.mcsnowflake.worktimer.data.WorkTimer.State.WORK_SESSION
import com.mcsnowflake.worktimer.model.TimerEvent
import com.mcsnowflake.worktimer.model.TimerEvent.*

class DoNotDisturbModeHandler(private val applicationContext: Context) {

    private var defaultInterruptionFilter = INTERRUPTION_FILTER_ALL

    fun process(event: TimerEvent) = when {
        event is Started && event.state == WORK_SESSION -> enableDND()
        (event is Finished || event is Terminated) && event.state == WORK_SESSION -> disableDND()
        else -> {
        }
    }

    private fun enableDND() {
        defaultInterruptionFilter = (applicationContext.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager).currentInterruptionFilter
        getSystemService(applicationContext, NotificationManager::class.java)
            ?.setInterruptionFilter(NotificationManager.INTERRUPTION_FILTER_PRIORITY)
    }

    private fun disableDND() {
        getSystemService(applicationContext, NotificationManager::class.java)
            ?.setInterruptionFilter(defaultInterruptionFilter)
    }
}
