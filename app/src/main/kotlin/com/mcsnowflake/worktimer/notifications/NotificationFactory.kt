package com.mcsnowflake.worktimer.notifications

import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.ContentResolver
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.media.AudioAttributes
import android.net.Uri
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat
import com.mcsnowflake.worktimer.MainActivity
import com.mcsnowflake.worktimer.R
import com.mcsnowflake.worktimer.TimerService
import com.mcsnowflake.worktimer.data.WorkTimer.State.*
import com.mcsnowflake.worktimer.model.TimerConfig.Companion.DEFAULT_HYDRATION_MODE
import com.mcsnowflake.worktimer.model.TimerConfig.Companion.DEFAULT_OVERTIME_MODE
import com.mcsnowflake.worktimer.model.TimerConfig.Companion.HYDRATION_MODE_KEY
import com.mcsnowflake.worktimer.model.TimerConfig.Companion.OVERTIME_MODE_KEY
import com.mcsnowflake.worktimer.model.TimerEvent
import com.mcsnowflake.worktimer.model.TimerEvent.*
import com.mcsnowflake.worktimer.resourcehandling.TextHandler.getInfoText
import com.mcsnowflake.worktimer.resourcehandling.TextHandler.getTitle
import com.mcsnowflake.worktimer.resourcehandling.TimerAction
import kotlin.math.abs
import kotlin.time.DurationUnit
import kotlin.time.ExperimentalTime
import kotlin.time.toDuration
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.FlowPreview

@ExperimentalCoroutinesApi
@ExperimentalTime
@FlowPreview
class NotificationFactory(
    private val applicationContext: Context,
    private val preferences: SharedPreferences
) {

    private val notificationManager = NotificationManagerCompat.from(applicationContext)
    private val pendingIntent = PendingIntent.getActivity(
        applicationContext,
        PendingRequestCode,
        Intent(applicationContext, MainActivity::class.java).addCategory(Intent.CATEGORY_LAUNCHER).setAction("android.intent.action.MAIN"),
        PendingIntent.FLAG_IMMUTABLE
    )

    fun getStartNotification(): Notification = NotificationCompat.Builder(applicationContext, AlarmChannelID)
        .setContentIntent(pendingIntent)
        .setSilent(true)
        .setSmallIcon(R.drawable.ic_logo)
        .setContentTitle("WorkTimer is starting")
        .build()

    fun process(event: TimerEvent) =
        when (event) {
            is Progress -> {
                if (shouldHydrate(event)) hydrate()
                progress(event)
            }
            is Finished -> alarm(event)
            is Terminated -> notificationManager.notify(TimerNotificationID, buildStoppedNotification())
            else -> {
            }
        }

    private fun shouldHydrate(update: Progress) = preferences.getBoolean(HYDRATION_MODE_KEY, DEFAULT_HYDRATION_MODE) &&
        (
            (update.state == WORK_SESSION && abs(update.progress - 50) < 40 && update.remainingTime.toDouble(DurationUnit.MINUTES) % 15 == 0.0) ||
                (update.state != WORK_SESSION && update.remainingTime == 2.toDuration(DurationUnit.MINUTES))
            )

    private fun hydrate() = notificationManager.notify(HydrationNotificationID, buildHydrateNotification())

    private fun alarm(event: Finished) = notificationManager.notify(TimerNotificationID, buildAlarmNotification(event))

    private fun progress(update: Progress) = notificationManager.notify(TimerNotificationID, buildProgressNotification(update))

    private fun buildProgressNotification(update: Progress): Notification {
        val builder = NotificationCompat.Builder(applicationContext, AlarmChannelID)
            .setContentIntent(pendingIntent)
            .setSilent(true)
            .setSmallIcon(R.drawable.ic_logo)
            .addAction(buildNotificationAction(TimerAction.STOP))
            .setCategory(Notification.CATEGORY_PROGRESS)
            .setContentTitle(getTitle(update))
            .setContentText("${getInfoText(update)} remaining")
            .setProgress(100, update.progress, false)

        return when (update.state) {
            WORK_SESSION -> builder.addAction(buildNotificationAction(TimerAction.PAUSE))
            SHORT_BREAK, LONG_BREAK -> builder.addAction(buildNotificationAction(TimerAction.WORK))
        }.build()
    }

    private fun buildAlarmNotification(update: TimerEvent): Notification {
        val builder = NotificationCompat.Builder(applicationContext, AlarmChannelID)
            .setContentIntent(pendingIntent)
            .setSmallIcon(R.drawable.ic_logo)
            .addAction(buildNotificationAction(TimerAction.STOP))
            .addAction(buildNotificationAction(TimerAction.RESUME))
            .setAllowSystemGeneratedContextualActions(false)
            .setCategory(Notification.CATEGORY_ALARM)
            .setContentTitle(getTitle(update))
            .setContentText(getInfoText(update))

        return if (preferences.getBoolean(OVERTIME_MODE_KEY, DEFAULT_OVERTIME_MODE))
            builder.addAction(buildNotificationAction(TimerAction.MORE)).build()
        else builder.build()
    }

    private fun buildStoppedNotification() = NotificationCompat.Builder(applicationContext, AlarmChannelID)
        .setContentIntent(pendingIntent)
        .setTimeoutAfter(100)
        .setAutoCancel(true)
        .setSmallIcon(R.drawable.ic_logo)
        .setCategory(Notification.CATEGORY_MESSAGE)
        .setContentTitle("Timer stopped")
        .build()

    private fun buildHydrateNotification() = NotificationCompat.Builder(applicationContext, HydrateChannelID)
        .setAutoCancel(true)
        .setTimeoutAfter(10_000)
        .setSmallIcon(R.drawable.ic_logo)
        .setCategory(Notification.CATEGORY_ALARM)
        .setContentTitle("Remember to hydrate yourself") // TODO refactor out string representations
        .build()

    private fun buildNotificationAction(action: TimerAction): NotificationCompat.Action = NotificationCompat.Action.Builder(
        TimerAction.getActionIcon(action),
        TimerAction.getActionTitle(action),
        PendingIntent.getService(applicationContext, PendingRequestCode, TimerService.getIntent(applicationContext, action.serviceAction), PendingIntent.FLAG_IMMUTABLE)
    ).build()

    companion object {
        const val PendingRequestCode = 52334
        const val TimerNotificationID = 2345
        const val HydrationNotificationID = 5346
        const val AlarmChannelID = "47657"
        const val HydrateChannelID = "5253432"

        fun createChannelsWith(applicationContext: Context) {
            NotificationManagerCompat.from(applicationContext)
                .createNotificationChannel(
                    NotificationChannel(
                        AlarmChannelID,
                        "WorkTimer Notification Channel",
                        NotificationManager.IMPORTANCE_HIGH
                    ).apply {
                        lockscreenVisibility = NotificationCompat.VISIBILITY_PUBLIC
                        setShowBadge(false)
                    }
                )
            NotificationManagerCompat.from(applicationContext)
                .createNotificationChannel(
                    NotificationChannel(
                        HydrateChannelID,
                        "Hydration Reminder Channel",
                        NotificationManager.IMPORTANCE_HIGH
                    ).apply {
                        lockscreenVisibility = NotificationCompat.VISIBILITY_PUBLIC
                        setShowBadge(false)
                        setSound(
                            Uri.parse(ContentResolver.SCHEME_ANDROID_RESOURCE + "://" + applicationContext.packageName + "/" + R.raw.drop),
                            AudioAttributes.Builder()
                                .setContentType(AudioAttributes.CONTENT_TYPE_SONIFICATION)
                                .setUsage(AudioAttributes.USAGE_NOTIFICATION_RINGTONE)
                                .build()
                        )
                    }
                )
        }
    }
}
