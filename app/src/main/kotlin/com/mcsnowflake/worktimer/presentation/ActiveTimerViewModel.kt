package com.mcsnowflake.worktimer.presentation

import android.content.SharedPreferences
import android.view.View
import androidx.lifecycle.*
import com.mcsnowflake.worktimer.data.WorkTimer
import com.mcsnowflake.worktimer.model.TimerConfig.Companion.DEFAULT_OVERTIME_MODE
import com.mcsnowflake.worktimer.model.TimerConfig.Companion.OVERTIME_MODE_KEY
import com.mcsnowflake.worktimer.model.TimerEvent
import com.mcsnowflake.worktimer.model.TimerEvent.*
import com.mcsnowflake.worktimer.resourcehandling.TextHandler.getInfoText
import com.mcsnowflake.worktimer.resourcehandling.TextHandler.getTitle
import com.mcsnowflake.worktimer.resourcehandling.TimerAction.*
import com.mcsnowflake.worktimer.resourcehandling.TimerAction.Companion.getActionTitle
import kotlin.time.ExperimentalTime
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.FlowPreview
import kotlinx.coroutines.channels.BroadcastChannel
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.flow.*

@ExperimentalCoroutinesApi
@FlowPreview
class ActiveTimerViewModel(private val preferences: SharedPreferences) : ViewModel() {

    // auto binding to timer service and updating this variable is done in the main activity
    val isTimerServiceRunning = MutableLiveData<Boolean>()

    private val updates = BroadcastChannel<TimerEvent>(Channel.BUFFERED)

    fun setUpdates(updateFlow: Flow<TimerEvent>) {
        updateFlow.filterNot { it is Terminated }.onEach { updates.send(it) }.launchIn(viewModelScope)
    }

    // element content
    val headerText: LiveData<String> = updates.asFlow().map { getTitle(it) }.distinctUntilChanged().asLiveData()

    @ExperimentalTime
    val infoText: LiveData<String> = updates.asFlow().map { getInfoText(it) }.distinctUntilChanged().asLiveData()

    val progress: LiveData<Int> = updates.asFlow().filterIsInstance<Progress>().map { it.progress }.distinctUntilChanged().asLiveData()

    // button labels
    @ExperimentalTime
    val nextButtonLabel: LiveData<String> = updates.asFlow().map {
        when (it) {
            is Started, is Progress ->
                if (it.state == WorkTimer.State.WORK_SESSION) getActionTitle(PAUSE)
                else getActionTitle(WORK)
            is Finished, is Terminated ->
                getActionTitle(RESUME)
        }
    }.distinctUntilChanged().asLiveData()

    // element visibilities
    val alarmView: LiveData<Int> = updates.asFlow().map {
        when (it) {
            is Started, is Progress -> View.INVISIBLE
            is Finished -> View.VISIBLE
            else -> View.INVISIBLE
        }
    }.distinctUntilChanged().asLiveData()

    val stateView: LiveData<Int> = updates.asFlow().map {
        when (it) {
            is Started, is Progress -> View.VISIBLE
            is Finished -> View.INVISIBLE
            else -> View.VISIBLE
        }
    }.distinctUntilChanged().asLiveData()

    val moreButton: LiveData<Int> = updates.asFlow().map {
        when (it) {
            is Finished ->
                if (preferences.getBoolean(OVERTIME_MODE_KEY, DEFAULT_OVERTIME_MODE)) View.VISIBLE
                else View.GONE
            else -> View.GONE
        }
    }.onStart { View.INVISIBLE }.distinctUntilChanged().asLiveData()
}
