package com.mcsnowflake.worktimer.presentation

import android.app.NotificationManager
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.provider.Settings
import android.view.MenuItem
import androidx.navigation.findNavController
import androidx.preference.PreferenceFragmentCompat
import androidx.preference.SwitchPreferenceCompat
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.mcsnowflake.worktimer.R

class SettingsFragment : PreferenceFragmentCompat() {

    override fun onCreatePreferences(savedInstanceState: Bundle?, rootKey: String?) {

        setHasOptionsMenu(true)

        setPreferencesFromResource(R.xml.preferences, rootKey)

        findPreference<SwitchPreferenceCompat>("dnd_mode")?.setOnPreferenceClickListener {
            val mNotificationManager = requireActivity().getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager

            if (!mNotificationManager.isNotificationPolicyAccessGranted) {
                MaterialAlertDialogBuilder(requireContext())
                    .setTitle("Permissions required")
                    .setMessage("In order to change the DND mode, the app needs according permissions. You will be forwarded to the settings page.")
                    .setPositiveButton("OK") { _, _ -> startActivity(Intent(Settings.ACTION_NOTIFICATION_POLICY_ACCESS_SETTINGS)) }
                    .setNegativeButton("Nope") { _, _ -> }
                    .show()
            }
            true
        }
        findPreference<SwitchPreferenceCompat>("dnd_mode")?.setOnPreferenceChangeListener { _, newValue ->
            val mNotificationManager = requireActivity().getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
            newValue is Boolean && mNotificationManager.isNotificationPolicyAccessGranted
        }
    }

    override fun onOptionsItemSelected(item: MenuItem) = when (item.itemId) {
        android.R.id.home -> {
            requireActivity().findNavController(R.id.nav_host).navigateUp()
            true
        }
        else -> false
    }
}
