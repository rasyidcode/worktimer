package com.mcsnowflake.worktimer.presentation

import android.content.SharedPreferences
import androidx.databinding.InverseMethod
import androidx.lifecycle.ViewModel
import com.mcsnowflake.worktimer.model.TimerConfig.Companion.DEFAULT_LONG_BREAK_DURATION
import com.mcsnowflake.worktimer.model.TimerConfig.Companion.DEFAULT_OVERTIME_DURATION
import com.mcsnowflake.worktimer.model.TimerConfig.Companion.DEFAULT_SHORT_BREAK_DURATION
import com.mcsnowflake.worktimer.model.TimerConfig.Companion.DEFAULT_SHORT_BREAK_REPETITIONS
import com.mcsnowflake.worktimer.model.TimerConfig.Companion.DEFAULT_WORK_SESSION_DURATION
import com.mcsnowflake.worktimer.model.TimerConfig.Companion.LONG_BREAK_DURATION_KEY
import com.mcsnowflake.worktimer.model.TimerConfig.Companion.OVERTIME_DURATION_KEY
import com.mcsnowflake.worktimer.model.TimerConfig.Companion.SHORT_BREAK_DURATION_KEY
import com.mcsnowflake.worktimer.model.TimerConfig.Companion.SHORT_BREAK_REPETITIONS_KEY
import com.mcsnowflake.worktimer.model.TimerConfig.Companion.WORK_SESSION_DURATION_KEY

class SetupViewModel(private val preferences: SharedPreferences) : ViewModel() {

    var workSessionDuration = preferences.getInt(WORK_SESSION_DURATION_KEY, DEFAULT_WORK_SESSION_DURATION)
    var shortBreakDuration = preferences.getInt(SHORT_BREAK_DURATION_KEY, DEFAULT_SHORT_BREAK_DURATION)
    var longBreakDuration = preferences.getInt(LONG_BREAK_DURATION_KEY, DEFAULT_LONG_BREAK_DURATION)
    var shortBreakRepetitions = preferences.getInt(SHORT_BREAK_REPETITIONS_KEY, DEFAULT_SHORT_BREAK_REPETITIONS)
    private val overTime = preferences.getInt(OVERTIME_DURATION_KEY, DEFAULT_OVERTIME_DURATION)

    fun storeSettings() {
        with(preferences.edit()) {
            putInt(WORK_SESSION_DURATION_KEY, workSessionDuration)
            putInt(SHORT_BREAK_DURATION_KEY, shortBreakDuration)
            putInt(LONG_BREAK_DURATION_KEY, longBreakDuration)
            putInt(SHORT_BREAK_REPETITIONS_KEY, shortBreakRepetitions)
            putInt(OVERTIME_DURATION_KEY, overTime)
            commit()
        }
    }

    fun convertStringToInt(value: String): Int {
        return try {
            value.toInt()
        } catch (e: NumberFormatException) {
            -1
        }
    }

    @InverseMethod("convertStringToInt")
    fun convertIntToString(value: Int): String? {
        return value.toString()
    }
}
