package com.mcsnowflake.worktimer.presentation

import android.os.Bundle
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.mcsnowflake.worktimer.R
import com.mcsnowflake.worktimer.TimerService
import com.mcsnowflake.worktimer.databinding.ActiveTimerFragmentBinding
import kotlin.time.ExperimentalTime
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.FlowPreview
import org.koin.androidx.viewmodel.ext.android.sharedViewModel

@ExperimentalCoroutinesApi
@FlowPreview
@ExperimentalTime
class ActiveTimerFragment : Fragment() {

    private val serviceVM by sharedViewModel<ActiveTimerViewModel>()
    private lateinit var binding: ActiveTimerFragmentBinding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {

        setHasOptionsMenu(true)

        if (serviceVM.isTimerServiceRunning.value == false)
            findNavController().navigate(ActiveTimerFragmentDirections.toWelcomeFragment())

        serviceVM.isTimerServiceRunning.observe(
            viewLifecycleOwner,
            { update ->
                if (update == false) {
                    serviceVM.isTimerServiceRunning.removeObservers(viewLifecycleOwner)
                    findNavController().navigate(ActiveTimerFragmentDirections.toWelcomeFragment())
                }
            }
        )

        binding = ActiveTimerFragmentBinding.inflate(inflater, container, false)
        binding.lifecycleOwner = viewLifecycleOwner
        binding.viewmodel = serviceVM

        mapOf(
            binding.moreButton to TimerService.Action.MORE,
            binding.stopButton to TimerService.Action.STOP,
            binding.nextButton to TimerService.Action.NEXT
        ).forEach { (button, action) -> setOnClickListener(button, action) }

        return binding.root
    }

    private fun setOnClickListener(button: Button, action: TimerService.Action) = button.setOnClickListener { triggerTimerAction(action) }

    override fun onOptionsItemSelected(item: MenuItem) = when (item.itemId) {
        android.R.id.home -> {
            triggerTimerAction(TimerService.Action.STOP)
            true
        }
        else -> false
    }

    private fun triggerTimerAction(action: TimerService.Action) {
        when (action) {
            TimerService.Action.STOP -> {
                binding.alarmText.text = getString(R.string.stopping_hint)
                binding.alarmText.visibility = View.VISIBLE
                binding.stateView.visibility = View.INVISIBLE
                binding.buttons.visibility = View.INVISIBLE
                binding.header.visibility = View.INVISIBLE
            }
            else -> {}
        }
        val intent = TimerService.getIntent(requireActivity().applicationContext, action)
        requireActivity().startForegroundService(intent)
    }
}
