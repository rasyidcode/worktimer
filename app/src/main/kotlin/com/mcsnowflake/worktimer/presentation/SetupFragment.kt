package com.mcsnowflake.worktimer.presentation

import android.os.Bundle
import android.view.*
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.mcsnowflake.worktimer.R
import com.mcsnowflake.worktimer.TimerService
import com.mcsnowflake.worktimer.TimerService.Action.START
import com.mcsnowflake.worktimer.databinding.SetupFragmentBinding
import kotlin.time.ExperimentalTime
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.FlowPreview
import org.koin.androidx.viewmodel.ext.android.sharedViewModel
import org.koin.androidx.viewmodel.ext.android.viewModel

@ExperimentalCoroutinesApi
@FlowPreview
class SetupFragment : Fragment() {

    private val setupVM by viewModel<SetupViewModel>()
    private val serviceVM by sharedViewModel<ActiveTimerViewModel>()

    @ExperimentalTime
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {

        if (serviceVM.isTimerServiceRunning.value == true)
            findNavController().navigate(SetupFragmentDirections.toActiveFragment())

        serviceVM.isTimerServiceRunning.observe(
            viewLifecycleOwner,
            { update ->
                if (update == true) {
                    serviceVM.isTimerServiceRunning.removeObservers(viewLifecycleOwner)
                    findNavController().navigate(SetupFragmentDirections.toActiveFragment())
                }
            }
        )

        setHasOptionsMenu(true)

        val binding = SetupFragmentBinding.inflate(inflater, container, false)
        binding.lifecycleOwner = viewLifecycleOwner
        binding.viewmodel = setupVM
        binding.startButton.setOnClickListener {

            setupVM.storeSettings()

            val intent = TimerService.getIntent(requireActivity().applicationContext, START)
            requireActivity().startForegroundService(intent)
        }

        return binding.root
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.action_bar_menu, menu)
    }
}
