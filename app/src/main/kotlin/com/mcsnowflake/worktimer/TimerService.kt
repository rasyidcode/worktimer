package com.mcsnowflake.worktimer

import android.app.Service
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.os.Binder
import android.os.IBinder
import android.util.Log
import com.mcsnowflake.worktimer.TimerService.Action.*
import com.mcsnowflake.worktimer.data.Broadcaster
import com.mcsnowflake.worktimer.data.WorkTimer
import com.mcsnowflake.worktimer.model.TimerConfig.Companion.DEFAULT_DND_MODE
import com.mcsnowflake.worktimer.model.TimerConfig.Companion.DEFAULT_REACTIVE_SWITCH_MODE
import com.mcsnowflake.worktimer.model.TimerConfig.Companion.DND_MODE_KEY
import com.mcsnowflake.worktimer.model.TimerConfig.Companion.REACTIVE_SWITCH_MODE_KEY
import com.mcsnowflake.worktimer.model.TimerEvent
import com.mcsnowflake.worktimer.notifications.DoNotDisturbModeHandler
import com.mcsnowflake.worktimer.notifications.NotificationFactory
import com.mcsnowflake.worktimer.notifications.NotificationFactory.Companion.TimerNotificationID
import kotlin.time.ExperimentalTime
import kotlinx.coroutines.*
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import org.koin.android.ext.android.inject
import org.koin.core.parameter.parametersOf

@ExperimentalCoroutinesApi
@FlowPreview
@ExperimentalTime
class TimerService(
    private val serviceScope: CoroutineScope = CoroutineScope(Dispatchers.Default)
) : Service() {

    // a service must have a zero argument constructor, so we have to inject the dependencies
    private val timerEventsBroadcaster by inject<Broadcaster<TimerEvent>> { parametersOf(serviceScope) }
    private val timer by inject<WorkTimer> { parametersOf(timerEventsBroadcaster, serviceScope) }
    private val notificationFactory by inject<NotificationFactory>()
    private val doNotDisturbModeHandler by inject<DoNotDisturbModeHandler>()
    private val preferences by inject<SharedPreferences>()

    override fun onCreate() {
        super.onCreate()

        startForeground(TimerNotificationID, notificationFactory.getStartNotification())

        timerEventsBroadcaster
            .subscribe()
            .onEach { notificationFactory.process(it) }
            .launchIn(serviceScope)

        if (preferences.getBoolean(DND_MODE_KEY, DEFAULT_DND_MODE))
            timerEventsBroadcaster
                .subscribe()
                .onEach { doNotDisturbModeHandler.process(it) }
                .launchIn(serviceScope)

        if (!preferences.getBoolean(REACTIVE_SWITCH_MODE_KEY, DEFAULT_REACTIVE_SWITCH_MODE))
            timerEventsBroadcaster
                .subscribe()
                .onEach { if (it is TimerEvent.Finished) serviceScope.launch { timer.nextState() } }
                .launchIn(serviceScope)

        Log.d(javaClass.simpleName, "TimerService started")
    }

    override fun onStartCommand(intent: Intent, flags: Int, startId: Int): Int {
        Log.d(javaClass.simpleName, "Intend received: ${intent.action}")
        kotlin.runCatching { executeIntentAction(valueOf(intent.action!!)) }
        return super.onStartCommand(intent, flags, startId)
    }

    override fun onDestroy() {
        serviceScope.cancel()
        Log.d(javaClass.simpleName, "TimerService stopped")
        super.onDestroy()
    }

    private fun executeIntentAction(action: Action) {
        when (action) {
            START -> serviceScope.launch { timer.start() }
            NEXT -> serviceScope.launch { timer.nextState() }
            MORE -> serviceScope.launch { timer.moreTime() }
            STOP -> serviceScope.launch {
                timer.stop()
                stopSelf()
            }
        }
    }

    inner class LocalBinder : Binder() {
        fun getTimerEvents() = timerEventsBroadcaster.subscribe()
    }

    override fun onBind(intent: Intent): IBinder {
        return LocalBinder()
    }

    companion object {
        fun getIntent(applicationContext: Context, action: Action) = getIntent(applicationContext).apply { this.action = action.name }
        fun getIntent(applicationContext: Context) = Intent(applicationContext, TimerService::class.java)
    }

    enum class Action {
        START, MORE, NEXT, STOP
    }
}
