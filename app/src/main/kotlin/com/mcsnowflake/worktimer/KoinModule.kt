package com.mcsnowflake.worktimer

import androidx.preference.PreferenceManager
import com.mcsnowflake.worktimer.data.Broadcaster
import com.mcsnowflake.worktimer.data.EventReceiver
import com.mcsnowflake.worktimer.data.WorkTimer
import com.mcsnowflake.worktimer.model.TimerConfig.Companion.getConfig
import com.mcsnowflake.worktimer.model.TimerEvent
import com.mcsnowflake.worktimer.notifications.DoNotDisturbModeHandler
import com.mcsnowflake.worktimer.notifications.NotificationFactory
import com.mcsnowflake.worktimer.presentation.ActiveTimerViewModel
import com.mcsnowflake.worktimer.presentation.SetupViewModel
import kotlin.time.ExperimentalTime
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.FlowPreview
import org.koin.android.ext.koin.androidContext
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

@ExperimentalCoroutinesApi
@FlowPreview
@ExperimentalTime
val appModule = module {

    factory { PreferenceManager.getDefaultSharedPreferences(androidContext()) }
    factory { (scope: CoroutineScope) -> Broadcaster<TimerEvent>(scope) }
    factory { (eventReceiver: EventReceiver<TimerEvent>, scope: CoroutineScope) ->
        WorkTimer(eventReceiver, PreferenceManager.getDefaultSharedPreferences(androidContext()).getConfig(), scope)
    }
    factory { NotificationFactory(androidContext(), get()) }
    factory { DoNotDisturbModeHandler(androidContext()) }
    factory { TimerService() }

    viewModel { ActiveTimerViewModel(get()) }
    viewModel { SetupViewModel(get()) }
}
