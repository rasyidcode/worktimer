FROM openjdk:11-jdk

ARG ANDROID_COMPILE_SDK="30"
ARG ANDROID_BUILD_TOOLS="30.0.2"
ARG ANDROID_SDK_TOOLS="7302050"

ENV PATH "$PATH:$PWD/android-sdk-linux/platform-tools/"
ENV ANDROID_HOME "$PWD/android-sdk-linux"
ENV JAVA_HOME "/usr/lib/jvm/java-11-openjdk-amd64"

RUN apt-get update -qq > /dev/null && \
    apt-get install -qq --no-install-recommends \
    wget tar unzip lib32stdc++6 lib32z1 && \
    wget --quiet --output-document=android-sdk.zip \
    https://dl.google.com/android/repository/commandlinetools-linux-${ANDROID_SDK_TOOLS}_latest.zip  && \
    unzip -d android-sdk-linux android-sdk.zip
RUN echo y | android-sdk-linux/cmdline-tools/bin/sdkmanager --sdk_root=${ANDROID_HOME} \
    "platforms;android-${ANDROID_COMPILE_SDK}" >/dev/null  && \
    echo y | android-sdk-linux/cmdline-tools/bin/sdkmanager --sdk_root=${ANDROID_HOME} \
    "platform-tools" >/dev/null && \
    echo y | android-sdk-linux/cmdline-tools/bin/sdkmanager --sdk_root=${ANDROID_HOME} \
    "build-tools;${ANDROID_BUILD_TOOLS}" >/dev/null && \
    yes | android-sdk-linux/cmdline-tools/bin/sdkmanager --sdk_root=${ANDROID_HOME} --licenses
