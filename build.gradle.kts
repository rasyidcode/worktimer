buildscript {

    val navVersion = "2.3.5"
    val ktlintVersion = "10.0.0"

    repositories {
        google()
        mavenCentral()
        maven("https://plugins.gradle.org/m2/")
    }
    dependencies {
        classpath("com.android.tools.build:gradle:4.2.1")
        classpath(kotlin("gradle-plugin", version = "1.4.32"))
        classpath("androidx.navigation:navigation-safe-args-gradle-plugin:$navVersion")
        classpath("org.jlleitschuh.gradle:ktlint-gradle:$ktlintVersion")
        classpath("org.jacoco:org.jacoco.core:0.8.7")
    }
}
allprojects {
    repositories {
        mavenCentral()
        google()
    }
}

plugins {
    id("org.jlleitschuh.gradle.ktlint") version "10.0.0"
    id("de.jansauer.printcoverage") version "2.0.0"
    jacoco
}
subprojects {

    apply(plugin = "org.jlleitschuh.gradle.ktlint") // Version should be inherited from parent
    ktlint {
        android.set(true)
        outputToConsole.set(true)
        outputColorName.set("RED")
        reporters {
            reporter(org.jlleitschuh.gradle.ktlint.reporter.ReporterType.HTML)
        }
        filter {
            exclude("**/generated/**")
            include("**/kotlin/**")
        }
    }

    apply(plugin = "jacoco")
    jacoco {
        toolVersion = "0.8.7"
        reportsDirectory.set(file("$buildDir/reports/jacoco"))
    }

    tasks {

        create(name = "testCoverage", type = JacocoReport::class.java) {

            reports {
                xml.isEnabled = true
                xml.destination = file("${parent!!.buildDir}/reports/jacoco/test/jacocoTestReport.xml")
                html.isEnabled = true
                html.destination = file("${parent!!.buildDir}/reports/jacoco/html")
            }

            val unwantedClassFiles = PatternSet().include(
                listOf(
                    "**/R.class",
                    "**/R$*.class",
                    "**/*FragmentDirections*",
                    "**/*KoinModule*",
                    "**/*TimerApplication*",
                    "**/BuildConfig.*",
                    "**/Manifest*.*",
                    "**/*Test*.*",
                    "android/**/*.*",
                    "**/*generated*/**",
                    "**/data/models/*",
                    "**/*\$\$special\$\$inlined\$activityViewModels\$*",
                    "**/*\$\$special\$\$inlined\$inject\$*",
                    "**/*\$\$special\$\$inlined\$viewModels\$*",
                    "**/*\$\$special\$\$inlined\$transform\$*",
                    "**/*\$\$inlined\$filterIsInstance\$",
                    "**/*\$\$inlined\$map*",
                    "**/*\$\$inlined\$collect*",
                    "**/*\$\$inlined\$CoroutineExceptionHandler\$*"
                )
            )
            val codeCoverageIncludes = PatternSet().include(
                "jacoco/testDebugUnitTest.exec",
                "outputs/code-coverage/debugAndroidTest/connected/*.ec"
            )

            val classesDebugTree = fileTree("$buildDir/tmp/kotlin-classes/debug/")
            val classesToExclude = classesDebugTree.matching(unwantedClassFiles)

            val debugTree = classesDebugTree.minus(classesToExclude)
            sourceDirectories.setFrom(project.file("src/main/kotlin"))
            classDirectories.setFrom(debugTree)
            executionData.setFrom(fileTree("$buildDir").matching(codeCoverageIncludes))
        }
    }
    tasks.withType<Test> {
        useJUnitPlatform()
        configure<JacocoTaskExtension> {
            // isIncludeNoLocationClasses = true
        }
        finalizedBy("testCoverage")
    }
}
tasks.register("clean", Delete::class) {
    delete(rootProject.buildDir)
}
