## WorkTimer

This app supports you in structuring your work time into productivity and relax sessions by following the <a href="https://en.wikipedia.org/wiki/Pomodoro_Technique">pomodoro principle</a>.

Additionally, it can remind you to drink and can turn on DoNotDisturb mode when you are working.

[<img src="https://fdroid.gitlab.io/artwork/badge/get-it-on.png"
    alt="Get it on F-Droid"
    height="80">](https://f-droid.org/packages/com.mcsnowflake.worktimer)


Contributions welcome.

